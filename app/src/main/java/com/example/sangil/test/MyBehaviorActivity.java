package com.example.sangil.test;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.sangil.test.databinding.ActivityMyBehaviorBinding;

public class MyBehaviorActivity extends AppCompatActivity {

    ActivityMyBehaviorBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_behavior);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        startActivity(new Intent(this,TimeLineActivity.class));
    }
}
