package com.example.sangil.test;

import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.util.Log;
import android.view.View;

import com.example.sangil.test.custom.timelinechart.TimelineChartView;
import com.example.sangil.test.databinding.ActivityTimeLineBinding;
import com.github.eunsiljo.timepercentchartlib.data.LegendData;
import com.github.eunsiljo.timepercentchartlib.data.TimeChartData;
import com.github.eunsiljo.timepercentchartlib.data.TimeData;


import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;
import org.joda.time.Weeks;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import lt.neworld.spanner.Spanner;
import lt.neworld.spanner.Spans;

public class TimeLineActivity extends AppCompatActivity  {

    ActivityTimeLineBinding binding;
    private final SimpleDateFormat DATETIME_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    private final NumberFormat NUMBER_FORMATTER = new DecimalFormat("#0.00");
    private final String[] COLUMN_NAMES = {"timestamp", "Serie 1", "Serie 2", "Serie 3"};
    InMemoryCursor mCursor;


    // ********* time percent
    private List<String> header =  Arrays.asList("걷기");
    private long mNow = 0;

    private long getMillis(String day){
        DateTime date = getDateTimePattern().parseDateTime(day);
        return date.getMillis();
    }

    private DateTimeFormatter getDateTimePattern(){
        return DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    }

    private void initData(){
        //initSamples();

        DateTime now = DateTime.now();
        mNow = now.withTimeAtStartOfDay().getMillis();
        binding.timeChart.setStartHour(0);
        //timeChart.setTimeChart(getMillis("2017-11-10 00:00:00"), mSamples);
        binding.timeChart.setTimeChart(mNow, getSamples(mNow, header));
        binding.timeChart.showLegend(false);
    }

    private ArrayList<TimeChartData> getSamples(long date, List<String> headers){
        TypedArray colors_chart = getResources().obtainTypedArray(R.array.colors_chart);

        ArrayList<TimeChartData> charts = new ArrayList<>();
        for(int i=0; i<headers.size(); i++){
            ArrayList<TimeData> values = new ArrayList<>();
            DateTime start = new DateTime(date);
            DateTime end = start.plusMinutes((int)((Math.random() * 10) + 1) * 30);
            for(int j=0; j<7; j++){
                values.add(new TimeData(start.getMillis(), end.getMillis()));

                start = end.plusMinutes((int)((Math.random() * 10) + 1) * 10);
                end = start.plusMinutes((int)((Math.random() * 10) + 1) * 30);
            }
            charts.add(new TimeChartData(colors_chart.getResourceId(new Random().nextInt(4), 0), new LegendData(headers.get(i)), values));
        }

        return charts;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_time_line);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initData();

        mCursor = day();
        binding.graph.observeData(mCursor);

        binding.day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCursor = day();
                binding.graph.getmObject().clear();
                binding.graph.observeData(mCursor);
            }
        });

        binding.week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCursor = week();
                binding.graph.getmObject().clear();
                binding.graph.observeData(mCursor);
            }
        });

        binding.month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCursor = month();
                binding.graph.getmObject().clear();
                binding.graph.observeData(mCursor);
            }
        });

        binding.graph.setOnClickItemListener(new TimelineChartView.OnClickItemListener() {
            @Override
            public void onClickItem(TimelineChartView.Item item, int serie) {
                String timestamp = DATETIME_FORMATTER.format(item.mTimestamp);
                Log.e("aaaa1","aaaa");
/*                Toast.makeText(TimeLineActivity.this, "onClickItem => " + timestamp
                        + ", serie: " + serie + " : " + binding.graph.getmObject().get(item.mTimestamp), Toast.LENGTH_SHORT).show();*/
                binding.graph.smoothScrollTo(item.mTimestamp);
            }
        });

        binding.graph.addOnSelectedItemChangedListener(new TimelineChartView.OnSelectedItemChangedListener() {
            @Override
            public void onSelectedItemChanged(TimelineChartView.Item selectedItem, boolean fromUser) {
                String timestamp = DATETIME_FORMATTER.format(selectedItem.mTimestamp);
                Log.e("aaaa2","aaaa");
                /*   Toast.makeText(TimeLineActivity.this, "onClickItem => " + timestamp
                        + " : " + binding.graph.getmObject().get(selectedItem.mTimestamp), Toast.LENGTH_SHORT).show();*/
            }

            @Override
            public void onNothingSelected() {
               /* mTimestamp.setText("-");
                for (TextView v : mSeries) {
                    v.setText("-");
                }*/
            }
        });

        binding.graph.addOnColorPaletteChangedListener(new TimelineChartView.OnColorPaletteChangedListener() {
            @Override
            public void onColorPaletteChanged(int[] palette) {
                // **************** 색깔 초기화

            }
        });
        Spannable spannable1 = new Spanner()
                .append("3시간 28분\n",Spans.sizeDP(13))
                .append("걷기",Spans.sizeDP(13),Spans.foreground(Color.GRAY))
                .span("시간", Spans.sizeDP(10))
                .span("분", Spans.sizeDP(10));
        binding.walk.setText(spannable1);

        Spannable spannable2 = new Spanner()
                .append("1시간 20분\n",Spans.sizeDP(13))
                .append("휴식",Spans.sizeDP(13),Spans.foreground(Color.GRAY))
                .span("시간", Spans.sizeDP(10))
                .span("분", Spans.sizeDP(10));
        binding.rest.setText(spannable2);

        Spannable spannable3 = new Spanner()
                .append("30분\n",Spans.sizeDP(13))
                .append("운전",Spans.sizeDP(13),Spans.foreground(Color.GRAY))
                .span("시간", Spans.sizeDP(10))
                .span("분", Spans.sizeDP(10));
        binding.drive.setText(spannable3);
    }

    public static SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

    public int getBetweenWeeksCount(Calendar start, Calendar end){
        return Weeks.weeksBetween(new DateTime(start), new DateTime(end)).getWeeks();
    }

    public int getBetweenDaysCount(Calendar start, Calendar end){
        return Days.daysBetween(new DateTime(start), new DateTime(end)).getDays();
    }

    public int getBetweenMonthsCount(Calendar start, Calendar end){
        return Months.monthsBetween(new DateTime(start), new DateTime(end)).getMonths();
    }

    public InMemoryCursor week() {
        InMemoryCursor mCursor = new InMemoryCursor(COLUMN_NAMES);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, 6);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        //calendar.setTime(calendarDate);

        Calendar end = Calendar.getInstance();
        int weeks_count = getBetweenWeeksCount(calendar,end);

        for (int i = 0; i < weeks_count + 1; i++) {
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            String s = format.format(calendar.getTimeInMillis());
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
            String e = format.format(calendar.getTimeInMillis());
            mCursor.add(createItem(calendar.getTimeInMillis(),s+" ~ \n"+e));
            calendar.add(Calendar.WEEK_OF_MONTH, 1);
        }
        return mCursor;
    }

    public InMemoryCursor day() {
        InMemoryCursor mCursor = new InMemoryCursor(COLUMN_NAMES);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, 6);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        //calendar.setTime(startDate);

        Calendar end = Calendar.getInstance();
        int days_count = getBetweenDaysCount(calendar,end);

        String footer;
        for (int i = 0; i < days_count+1; i++) {
            int month = calendar.get(Calendar.MONTH)+1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            if(month == 1 && day == 1){
                footer = calendar.get(Calendar.YEAR)+"\n"+month+"/"+day;
            }else{
                footer = month+"/"+day;
            }
            mCursor.add(createItem(calendar.getTimeInMillis(),footer));
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        return mCursor;
    }


    public InMemoryCursor month() {
        InMemoryCursor mCursor = new InMemoryCursor(COLUMN_NAMES);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, 6);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        //start.setTime(startDate);

        Calendar end = Calendar.getInstance();
        int month_count = getBetweenMonthsCount(calendar,end);

        String footer;
        for (int i = 0; i < month_count+1; i++) {
            int month = calendar.get(Calendar.MONTH)+1;
            if(month == 1){
                footer = calendar.get(Calendar.YEAR)+"\n"+month+"월";
            }else{
                footer = month+"월";
            }
            mCursor.add(createItem(calendar.getTimeInMillis(),footer));
            calendar.add(Calendar.MONTH, 1);
        }
        return mCursor;
    }


   /* public InMemoryCursor Item1() {
        mCursor = new InMemoryCursor(COLUMN_NAMES);
        Calendar mStart = Calendar.getInstance();
        mStart.set(Calendar.HOUR_OF_DAY, 0);
        mStart.set(Calendar.MINUTE, 0);
        mStart.set(Calendar.SECOND, 0);
        mStart.set(Calendar.MILLISECOND, 0);


        mStart.add(Calendar.MONTH, 1);
        mCursor.add(createItem(mStart.getTimeInMillis()));
        mStart.add(Calendar.MONTH, 1);
        mCursor.add(createItem(mStart.getTimeInMillis()));
        mStart.add(Calendar.MONTH, 1);
        mCursor.add(createItem(mStart.getTimeInMillis()));
        mStart.add(Calendar.MONTH, 1);
        mCursor.add(createItem(mStart.getTimeInMillis()));
        mStart.add(Calendar.MONTH, 1);
        mCursor.add(createItem(mStart.getTimeInMillis()));
        return mCursor;
    }*/

    int i = 0;

    private Object[] createItem(long timestamp,String footer) {
        Object[] item = new Object[COLUMN_NAMES.length];

        JSONObject object = new JSONObject();
        try {
            object.put("timestamp", timestamp);
            object.put("footer", footer);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        item[0] = object.toString();
        for (int i = 1; i < COLUMN_NAMES.length; i++) {
            item[i] = random(9999);
        }
        return item;
    }

    private int random(int max) {
        return (int) (Math.random() * (max + 1));
    }

}
